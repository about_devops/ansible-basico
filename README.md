![Screenshot](ansible.png)

# Ansible 

Desarrollado por Michael DeHaan, fue lanzado el 20 de febrero del 2012. Ansible es una plataforma de software libre para configurar y administrar ordenadores. Combina instalación multi-nodo (es decir: permite desplegar configuraciones de servidores y servicios por lotes), ejecuciones de tareas ad hoc y administración de configuraciones. Adicionalmente, Ansible es categorizado como una herramienta de orquestación. Gestiona nodos a través de SSH y no requiere ningún software remoto adicional (excepto Python 2.4 o posterior) para instalarlo. Dispone de módulos que trabajan sobre JSON y la salida estándar puede ser escrita en cualquier lenguaje. Nativamente utiliza YAML para describir configuraciones reusables de los sistemas


## 1. Instalación de ansible

Las vms tienen como sistema operativo ubuntu 19. Para instalar Ansible, agregaremos el repositorio e instalaremos los paquetes:

```
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt update
$ sudo apt install ansible
```

Listo. Valide que Ansible este instalado:

```
$ sudo ansible --version
ansible 2.9.10
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/root/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/dist-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.17 (default, Nov  7 2019, 10:07:09) [GCC 9.2.1 20191008]
```
Para realizar las pruebas con ansible, se crearon 3 virtualmachines (durante la capacitación se entragará los datos servidor y usuario).

* brbktdevopsgole1
* brbktdevopsgole2
* brbktdevopsgole3

## 2. Inventario de servidores

Todos los servidores ya tiene las llaves para ingresar a los 3 servidores de pruebas, por lo tanto primero debemos crear un inventario y configurar Ansible.

Por defecto el inventario se encuentra en /etc/ansible/hosts, pero tambien es posible crear inventarios personalizados en un ruta alternativa:

```
$ mkdir ansible; cd ansible
$ touch inventory
```

Y el archivo de configuración se encuentra en "/etc/ansible/ansible.cfg". Es posible copiar este archivo y modificarlo segun necesitemos para trabajar en projectos con configuraciones propias:

```
sudo cp /etc/ansible/ansible.cfg .
```

Listo, ya creamos nuestro inventario y copiamos el ansible.cfg a nuestros directorio de trabajo.

Editarmos "inventory" y agregamos nuestros servidores:

```
[group1]
brbktdevopsgole1
brbktdevopsgole2

[group2]
brbktdevopsgole3

[all:children]
group1
group2

[all:vars]
ansible_ssh_user=bt
```

El [group1] esta compuestos de 2 maquinas, [group2] solo tiene 1,  y [all:children] engloba todos los grupos. En [all:vars] definimos el usuario "bt" para ejecutar ansible.

Ahora hay que editar ansible.cfg para cambiar la ruta del inventario para "/home/bt/ansible/inventory":

![Screenshot](ansible_inv.png)

Y tambien descomentaremos un parametros "become=True" que permite anteponer un "sudo" en la ejecución de ansible:

![Screenshot](ansible_become.png)

Bien, ya configuramos algunas cosas, vamos a ejecutar algunos comandos!:

## 3. Ejecutando comandos ad-hoc

Un comando ad-hoc de Ansible utiliza la herramienta de línea de comandos /usr/bin/ansible para automatizar una sola tarea en uno o más nodos administrados. Los comandos ad-hoc son rápidos y fáciles, pero no son reutilizables. Los comandos ad-hoc demuestran la simplicidad y el poder de Ansible.


La estructura de comandos ad-hoc:

```
$ ansible [group] -m [modulo] -a [argumento]
```

Ejecute algunos comandos basicos

* PING al group1

```
$ ansible  group1 -m ping
brbktdevopsgole1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": false, 
    "ping": "pong"
}
brbktdevopsgole2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": false, 
    "ping": "pong"
}
```

Funciono!, usando el modulo "ping" los servidores del "group1" respondieron "pong". Si queremos que responda todos los grupos:

```
$ ansible  all -m ping
brbktdevopsgole2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": false, 
    "ping": "pong"
}
brbktdevopsgole1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": false, 
    "ping": "pong"
}
brbktdevopsgole3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": false, 
    "ping": "pong"
}
```

Perfecto, el inventario configurado funciona. Ahora ejecutemos algo mas interesante.

* Ejecutando comando remoto

```
$ ansible  all -a "uptime"
brbktdevopsgole1 | CHANGED | rc=0 >>
 01:59:00 up 35 min,  2 users,  load average: 0.10, 0.04, 0.18
brbktdevopsgole3 | CHANGED | rc=0 >>
 01:59:00 up  1:34,  1 user,  load average: 0.00, 0.00, 0.00
brbktdevopsgole2 | CHANGED | rc=0 >>
 01:59:00 up  1:36,  2 users,  load average: 0.04, 0.02, 0.00
```

Tenemos el uptime de cada equipo. 

* Instalando paquetes

Si quisieramos instalar algunos paquetes, podemos utilizar el modulo "apt"

```
$ ansible all -m apt -a "name=nmap state=latest"
```

La salida es muy larga. Utilizando "apt" instalamos nmap en su ultima versión del repositorio. Validemos comandos ejecutando "nmap localhost":

```
$ ansible  all -a "nmap localhost"
brbktdevopsgole3 | CHANGED | rc=0 >>
Starting Nmap 7.80 ( https://nmap.org ) at 2020-06-19 02:13 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000023s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.27 seconds
brbktdevopsgole2 | CHANGED | rc=0 >>
Starting Nmap 7.80 ( https://nmap.org ) at 2020-06-19 02:13 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000046s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.25 seconds
brbktdevopsgole1 | CHANGED | rc=0 >>
Starting Nmap 7.80 ( https://nmap.org ) at 2020-06-19 02:13 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000028s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.24 seconds
```

Bien, ahora para desinstalar. 

```
$ ansible all -m apt -a "name=nmap purge=yes"
```


Otra alternativa es agregar un "--check" para validar si nuestro comando ansible funcionará o tendra problemas:

```
$ ansible all -m apt -a "name=nginx state=latest" --check
```

La salida indica que es posible instalar.


* Copiando un archivo

Copie el archivo "/etc/passwd" de su servidor a los 3 servidores en "/tmp/"

```
$ ansible group1 -m copy -a "src=/etc/passwd dest=/tmp/passwd"
brbktdevopsgole2 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": true, 
    "checksum": "f2445b485220a1839569bf6639d531abd73a5efa", 
    "dest": "/tmp/passwd", 
    "gid": 0, 
    "group": "root", 
    "md5sum": "f09700c3dc4be67b3a8ccc46aced3f00", 
    "mode": "0644", 
    "owner": "root", 
    "size": 1692, 
    "src": "/home/bt/.ansible/tmp/ansible-tmp-1592533686.02-28638-226923057919683/source", 
    "state": "file", 
    "uid": 0
}
brbktdevopsgole1 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": true, 
    "checksum": "f2445b485220a1839569bf6639d531abd73a5efa", 
    "dest": "/tmp/passwd", 
    "gid": 0, 
    "group": "root", 
    "md5sum": "f09700c3dc4be67b3a8ccc46aced3f00", 
    "mode": "0644", 
    "owner": "root", 
    "size": 1692, 
    "src": "/home/bt/.ansible/tmp/ansible-tmp-1592533685.98-28636-211581105213979/source", 
    "state": "file", 
    "uid": 0
}

```

Validamos:

```
$ ansible group1 -a "ls -l /tmp/passwd"
brbktdevopsgole1 | CHANGED | rc=0 >>
-rw-r--r-- 1 root root 1692 Jun 19 02:28 /tmp/passwd
brbktdevopsgole2 | CHANGED | rc=0 >>
-rw-r--r-- 1 root root 1692 Jun 19 02:28 /tmp/passwd
```

* Crear directorio

El modulo "file" tambien sirve para crear directorio de forma recursiva. Cree una carpeta con el nombre de su vm en /tmp

```
ansible all -m file -a "dest=/tmp/devopsbasexx mode=755 owner=bt group=bt state=directory"
```

